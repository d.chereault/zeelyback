<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController{

    public function login(){
        $user = $this-> getUser();
        return $this->json([
            'username'=> $user->getUsername(),
            'roles'=> $user->getRoles(),
            'firstName'=> $user->getFirstName(),
            'lastName'=> $user->getLastName(),
            'adresse'=> $user->getAdresse(),
            'dDN'=> $user->getDDN(),
        ]);
    }
}