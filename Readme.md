Zeely(~BACK~) - Epreuve E4
==============================================

L'application
-------------


### Présentation

La nouvelle entreprise Zeely, au nombre de 14 salariés dont un Community manager, un chargé de communication, quatre développeurs et un designer, a été créée en 2021. Lionel MARSEAU préside avec Pierre LABLANCHE cette société anonyme, qui a pour but la conception d’une application web d’e-commerce permettant d’acheter des places de concert se trouvant en France


### Fonctionnalités

- [ ] Se connecter via un identifiant (exemple : pseudonyme / mot de passe)
- [ ] Application permettant de payer des places de concert (module de paiement non obligatoire)
- [ ] L’achat doit être accessible seulement aux membres
- [ ] L’application doit permettre d’acheter n’importe quelles places de concert en quantité voulue
- [ ] L’application doit mettre à disposition des concerts récents
- [ ] L’application doit permettre à l’utilisateur de retrouver ses achats à l’aide d’un historique de commandes
- [ ] L’application doit permettre à un admin de voir toutes les commandes réalisées
- [ ] La solution web doit différencier un utilisateur d’un administrateur
- [ ] L’application doit comporter une partie FrontEnd et une partie BackEnd
- [ ] Elle doit être rapide et simple

- [ ] Hashage du mot de passe en base de données
- [ ] Création d'une API 


### Technologies

*   Gestionnaire de version : **GIT** 
*   Framework **Symfony v.4.25.2**
*   Framework **Api Platform**
*   Langage de programmation : **PHP / yaml / css / JavaScript**
 
  
  
### Mise en route

*   ![](https://a.slack-edge.com/production-standard-emoji-assets/13.0/google-medium/1f4da.png) Ouvrir le dossier Back avec Visual Studio Code
*   ![](https://a.slack-edge.com/production-standard-emoji-assets/13.0/google-medium/1f3d7-fe0f.png) Entrer la commande 'cd zeely' puis 'symfony server:run' dans le terminal Bash 
*   ![](https://a.slack-edge.com/production-standard-emoji-assets/13.0/google-medium/1f4d1.png) se rendre sur https://localhost:8000


### Compte FTP du stockage de ressource
Voir fiche documentation 

### Base de données
Nom de la base : zeelyapi
  
