<?php

namespace ContainerZV18zAc;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder8a79c = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer1cd4c = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties488fb = [
        
    ];

    public function getConnection()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getConnection', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getMetadataFactory', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getExpressionBuilder', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'beginTransaction', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getCache', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getCache();
    }

    public function transactional($func)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'transactional', array('func' => $func), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->transactional($func);
    }

    public function commit()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'commit', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->commit();
    }

    public function rollback()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'rollback', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getClassMetadata', array('className' => $className), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'createQuery', array('dql' => $dql), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'createNamedQuery', array('name' => $name), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'createQueryBuilder', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'flush', array('entity' => $entity), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'clear', array('entityName' => $entityName), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->clear($entityName);
    }

    public function close()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'close', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->close();
    }

    public function persist($entity)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'persist', array('entity' => $entity), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'remove', array('entity' => $entity), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'refresh', array('entity' => $entity), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'detach', array('entity' => $entity), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'merge', array('entity' => $entity), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getRepository', array('entityName' => $entityName), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'contains', array('entity' => $entity), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getEventManager', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getConfiguration', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'isOpen', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getUnitOfWork', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getProxyFactory', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'initializeObject', array('obj' => $obj), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'getFilters', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'isFiltersStateClean', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'hasFilters', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return $this->valueHolder8a79c->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer1cd4c = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder8a79c) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder8a79c = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder8a79c->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, '__get', ['name' => $name], $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        if (isset(self::$publicProperties488fb[$name])) {
            return $this->valueHolder8a79c->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8a79c;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder8a79c;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8a79c;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder8a79c;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, '__isset', array('name' => $name), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8a79c;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder8a79c;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, '__unset', array('name' => $name), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8a79c;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder8a79c;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, '__clone', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        $this->valueHolder8a79c = clone $this->valueHolder8a79c;
    }

    public function __sleep()
    {
        $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, '__sleep', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;

        return array('valueHolder8a79c');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1cd4c = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1cd4c;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer1cd4c && ($this->initializer1cd4c->__invoke($valueHolder8a79c, $this, 'initializeProxy', array(), $this->initializer1cd4c) || 1) && $this->valueHolder8a79c = $valueHolder8a79c;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder8a79c;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder8a79c;
    }


}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
