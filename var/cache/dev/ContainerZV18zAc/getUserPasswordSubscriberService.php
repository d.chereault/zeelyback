<?php

namespace ContainerZV18zAc;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getUserPasswordSubscriberService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\EventSubscriber\UserPasswordSubscriber' shared autowired service.
     *
     * @return \App\EventSubscriber\UserPasswordSubscriber
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'src'.\DIRECTORY_SEPARATOR.'EventSubscriber'.\DIRECTORY_SEPARATOR.'UserPasswordSubscriber.php';

        return $container->privates['App\\EventSubscriber\\UserPasswordSubscriber'] = new \App\EventSubscriber\UserPasswordSubscriber(($container->services['security.password_encoder'] ?? $container->load('getSecurity_PasswordEncoderService')));
    }
}
