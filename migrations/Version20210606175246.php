<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210606175246 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, adresse VARCHAR(200) DEFAULT NULL, ddn DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE artiste CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE concert CHANGE id_artiste id_artiste INT NOT NULL, CHANGE date_concert date_concert DATETIME NOT NULL, CHANGE image image VARCHAR(200) DEFAULT NULL, CHANGE prix prix DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE od CHANGE numero numero VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE users ADD username VARCHAR(50) NOT NULL, CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE ddn ddn DATETIME DEFAULT NULL, CHANGE roles roles VARCHAR(20) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE `user`');
        $this->addSql('ALTER TABLE artiste CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('CREATE INDEX NomArtiste ON artiste (nom_artiste)');
        $this->addSql('ALTER TABLE concert CHANGE id_artiste id_artiste INT UNSIGNED NOT NULL, CHANGE date_concert date_concert DATE DEFAULT NULL, CHANGE image image VARCHAR(100) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE prix prix DOUBLE PRECISION UNSIGNED DEFAULT NULL');
        $this->addSql('CREATE INDEX idArtiste ON concert (id_artiste)');
        $this->addSql('ALTER TABLE `od` CHANGE numero numero VARCHAR(20) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE users DROP username, CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL, CHANGE ddn ddn DATE DEFAULT NULL, CHANGE roles roles VARCHAR(20) CHARACTER SET latin1 DEFAULT \'USER\' NOT NULL COLLATE `latin1_swedish_ci`');
    }
}
